requirejs.config({

	paths: {
		text:			'vendor/requirejs-text/text',
		jquery: 		['vendor/jquery/dist/jquery.min'],
		underscore: 	['vendor/underscore/underscore'],
		backbone:		['vendor/backbone'],
		handlebars:		['vendor/handlebars/handlebars'],
		yandexMaps: 	'http://api-maps.yandex.ru/2.1/?lang=ru_RU',

		router: 'application/core/router/main'
	},

	//	в этом блоке описываются библиотеки, в тело которых мы не можем добавить блок define, и соответственно RequireJS не будет ничего знать о зависимости этой библиотеки от других и не сможет ее загрузить. Самый простой случай — это jQuery плагины. Они не содержат define блоков и не могут быть использованы пока библиотека jQuery не будет полностью загружена
	shim: {
		'backbone': 	{exports: 'Backbone',		deps: ['underscore', 'jquery']},
		'underscore': 	{exports: '_'},
		'handlebars': 	{exports: 'Handlebars'}
	}
});

var PR = PR || {};
PR.TPL = {};

require(['backbone', 'handlebars'], function(Backbone, Handlebars, PR){
	require(['router']);
});