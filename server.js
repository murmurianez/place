var express = require('express');
var http = require('http');
var fs = require('fs');
var path = require('path');


var app = express();

app.set('views', path.join(__dirname, 'application/views'));

app.use(express.static(path.join(__dirname, 'application')));


app.get('/', function(request, response){
	var filePath = 'index.html';
	fs.readFile(filePath, function(error, data){
		response.writeHead(200, {'Content-type': 'text/html'});
		response.end(data);
	});
});


app.use(function(req, res){
	res.send(404, 'Sorry cant find that!');
});

app.listen(3000);