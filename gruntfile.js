module.exports = function (grunt) {
	'use strict';

	require('time-grunt')(grunt);

	//описываем конфигурацию
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'), //подгружаем package.json, чтобы использовать его данные

		watch: {
//			options: {
//				livereload: 2000
//			},
			html: {
				files: ['deploy/*.html'],
				tasks: ['concat']
			},
			scss: {
				files: ['static/scss/**/*.scss'],
				tasks: ['concat', 'sass', 'autoprefixer', 'csso', 'uncss']
			},
			js: {
				files: ['static/application/**/*.js'],
				tasks: ['requirejs']
			}
//			img: {
//				files: ['deploy/img'],
//				tasks: ['imagemin']
//			}
		},

		requirejs: {
			compile: {
				options: {
					baseUrl: "static/application",
					mainConfigFile: "static/main.js",
					out: "deploy/scripts.js"
				}
			}
		},

//		concat: {
//			options: {
//				separator: '\n\r'
//			},
//
//			dist: {
//				src: ['static/application/**/*.js'],
//				dest: 'deploy/scripts.js'
//			}
//		},

		sass: {
			dev: {
				files: {
					'deploy/styles.css': 'static/scss/styles.scss'
				}
			}
		},

		// Сохраняем svg в css
		grunticon: {
			myIcons: {
				files: [{
					expand: true,
					cwd: 'deploy/svg',
					src: ['*.svg'],
					dest: "deploy"
				}],
				options: {
					cssprefix: ".icon-",
					defaultWidth: "20px",
					defaultHeight: "20px"
				}
			}
		},

		//Подстановка вендорных префиксов в CSS
		autoprefixer: {
			options: {
				browsers: ['last 2 version', 'ie 8', 'opera 12']
			},
			dist: {
				src: 'deploy/styles.css'
			}
		},

		//Оптимизация CSS
		csso: {
			dist: {
				files: {
					'deploy/styles.css': ['deploy/styles.css']
				}
			}
		},

		//Удаление неиспользуемого CSS
		//Please note that the CSS parser used in the uncss module we rely on currently isn't able to work with complex selectors. For example [data-section=''] > section > [data-section-title] a. This may mean that at build time you run into exceptions such as TypeError: Cannot convert undefined or null to object. If this is the case, please consider moving these selectors to a separate stylesheet which the task does not run on.
		uncss: {
			dist: {
				files: {
					'deploy/styles.css': ['index.html']
				}
			}
		},

		//Измеряем скорость загрузки сайта
		pagespeed: {
			options: {
				nokey: true,
				url: "https://developers.google.com"
			},
			prod: {
				options: {
					url: "http://placeandwork.ru",
					locale: "ru_RU",
					strategy: "desktop", //mobile
					threshold: 80		 //Минимальный балл успешного прохождения теста
				}
			}
		},

		//Обширный отчёт по Front-end
		phantomas: {
			grunt: {
				options: {
//					assertions: {
//						'assetsWithQueryString': 3, //receive warning, when there are more than 3 assets with a query string
//						'bodyHTMLSize': 10500, 		//receive warning, when the bodyHTMLsize is bigger than 10500
//						'jsErrors': 0      			//receive warning, when JS errors appear
//					},

					indexPath: './phantomas/',
//					options: {
//						'timeout': 30
//					},
					url: 'http://placeandwork.ru'
				}
			}
		},

		yuidoc: {
			all: {
				name: '<%= pkg.name %>',
				description: '<%= pkg.description %>',
				version: '<%= pkg.version %>',
				url: '<%= pkg.homepage %>',
				options: {
					paths: ['resources'],
					outdir: './docs/'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
//	grunt.loadNpmTasks('grunt-contrib-uglify');
//	grunt.loadNpmTasks('grunt-contrib-htmlmin');
	grunt.loadNpmTasks('grunt-csso');
	grunt.loadNpmTasks('grunt-grunticon');
	grunt.loadNpmTasks('grunt-uncss');

	grunt.loadNpmTasks('grunt-requirejs');
	//	grunt.loadNpmTasks('grunt-contrib-compress');
//	grunt.loadNpmTasks('grunt-preprocess');
	grunt.loadNpmTasks('grunt-pagespeed');
	grunt.loadNpmTasks('grunt-phantomas');

	grunt.loadNpmTasks("grunt-contrib-yuidoc");

//		"browser-sync": "latest",
//		"grunt-karma": "latest",
//		"karma-qunit": "latest",
//		"karma-firefox-launcher": "latest",
//		"karma-ie-launcher": "latest",

	//регистрируем задачу
	grunt.registerTask('default', ['watch']); //задача по умолчанию, просто grunt
	grunt.registerTask('icons',['grunticon']);
	grunt.registerTask('deploy', ['grunticon', 'concat', 'sass', 'autoprefixer', 'csso']);
	grunt.registerTask("doc", ["yuidoc"]);
};